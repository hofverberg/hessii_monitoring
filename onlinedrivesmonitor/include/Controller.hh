/**
   \class onlinedrivesmonitor::Controller

   A class realising the DAQ controller state machine interface for the CT5 drives monitoring systems.

   \author Faical Ait Benkhali, Petter Hofverberg
*/

#ifndef ONLINEDRIVESMONITOR_CONTROLLER_H_
#define ONLINEDRIVESMONITOR_CONTROLLER_H_

#include "dash/Controller.hh"

#include "CoolingSystemMonitor.hh"
#include "VibrationSystemMonitor.hh"

namespace onlinedrivesmonitor {

  class Controller_i: virtual public Dash::Controller_i
  {
  public:
    Controller_i();
    ~Controller_i();

    Dash::Return ProcessEvent();

  protected:
    Dash::Return GettingReady();
    Dash::Return Configuring();
    Dash::Return Starting();
    Dash::Return Stopping();
    Dash::Return UnConfiguring();
    Dash::Return GoingToSafe();

  private:

    Dash::Return ConfigureHardware();
    Dash::Return ConfigureCoolingHardware();
    Dash::Return ConfigureVibrationHardware();
    void FreeHardware();
    void ProcessCoolingEvent();
    void ProcessVibrationEvent();
    bool IsVibrationMeasurementRun();

    CoolingSystemMonitor cooling_monitor_;
    VibrationSystemMonitor vibration_monitor_;

    bool temperature_warning_active_;

    //! rate for calling ProcessEvent, when not in a run
    const double monitorRate;
    //! rate for calling ProcessEvent, while run is performed
    const double runningRate;

    bool cooling_system_enabled_;
    bool vibration_system_enabled_;
  };

} // namespace

#endif
