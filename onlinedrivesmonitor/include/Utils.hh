/**
   \file onlinedrivesmonitor::Utils.hh

   Utility functions for converting between numerical values and strings.

   \author Faical Ait Benkhali, Petter Hofverberg
*/

#ifndef ONLINEDRIVESMONITOR_UTILS_H_
#define ONLINEDRIVESMONITOR_UTILS_H_

#include <stdint.h>
#include <vector>
#include <string>

namespace onlinedrivesmonitor {
/// Converts a string to a bool safely. Throws if the string was not a valid
/// bool.
bool StringToBool(const std::string &bool_as_string);
/// Converts a string to an int safely. Throws if the string was not a valid
/// int.
int StringToInt(const std::string &int_as_string);
/// Converts a string to a double safely. Throws if the string was not a valid
/// double.
double StringToDouble(const std::string &double_as_string);
/// Converts a string to a float safely. Throws if the string was not a valid
/// double.
float StringToFloat(const std::string &float_as_string);
/// Converts a vector to a string with the elements separated by a space
  std::string VectorToString(const std::vector<float>& vec, const int& n_digits);
/// Converts a string of several elements, each separated by a specific deliminator, to a vector containing all elements (without deliminator).
std::vector<std::string> StringToVector(const std::string& channels_str, const std::string& delim);


}  // namespace

#endif
