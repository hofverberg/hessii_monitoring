/**
   \class onlinedrivesmonitor::VibrationSystemMonitor

   A class for interacting with the CT5 vibration monitoring system

   \author Faical Ait Benkhali, Petter Hofverberg
*/

#ifndef ONLINEDRIVESMONITOR_VIBRATIONSYSTEMMONITOR_H_
#define ONLINEDRIVESMONITOR_VIBRATIONSYSTEMMONITOR_H_

#include <string>
#include <vector>
#include "drivesmonitor/VibrationMonitorEvent.hh"

namespace onlinedrivesmonitor {

class VibrationSystemMonitor
    {
    public:
      VibrationSystemMonitor();
      ~VibrationSystemMonitor();   
      void Configure();
      void Connect(); 
      void Deconnect();
      void StartMeasurement(const float& readdata_frequency);
      void ReadData(drivesmonitor::VibrationMonitorEvent *event);
      void StopMeasurement();
      bool IsMeasurementInProgress();

    private:
      void ConfigureFromDB();
      void DefaultConfiguration();
      std::vector<int> GetLJMChannelAddresses(const std::vector<std::string>& ch_names);
      std::string GetLJMErrorMessage(const int& ljm_error_code);
      void LoadLJMConfigurationFile();
      void SetLJMConstantsFilePath();
      void SetLJMTimeouts();

      int connection_;
      std::string ip_; 
      std::string port_;
      std::string device_type_;
      std::string connection_type_;
      
      int    connection_timeout_;  /// [ms]
      int    rw_timeout_; /// [ms]

      const static int MAX_NELEMENTS_READ;

      double des_scan_rate_; /// [scans/s]
      double act_scan_rate_; /// [scans/s]
      int scans_per_read_; // [scans]
      std::vector<std::string> channel_names_;
      double *data_;
};

} // namespace

#endif
