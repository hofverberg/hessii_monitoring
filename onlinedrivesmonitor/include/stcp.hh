
/*========================================================//

 date: Fri May  2 00:06:53 CEST 2008
 file: stcp.h created by ./make.header 
 from: stcp.c

 //========================================================*/

#ifndef INCLUDED_stcp_h
#define INCLUDED_stcp_h

struct StcpStruct
{
  int cid;
  int lid;
  int address;
  int port;
  int laddress;
  int lport;
  char name[256];
  int magic;
};
typedef struct StcpStruct *Stcp;

int StcpDebug(int i);
Stcp StcpInit(char *name, int qlen);
int StcpWait(Stcp id, int timeout);
Stcp StcpConnect(char *name, int timeout, int qlen);
char *StcpInfo(Stcp id, int i);
int StcpWrite(Stcp id, int n, void *data, int msec);
int StcpRead(Stcp id, int n, void *data, int msec);
int StcpClose(Stcp id);
int StcpShutdown(Stcp id);

#endif

