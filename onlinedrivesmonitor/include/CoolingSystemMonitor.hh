/**
   \class onlinedrivesmonitor::CoolingSystemMonitor

   A class for interacting with the CT5 cooling monitoring system

   \author Faical Ait Benkhali, Petter Hofverberg
*/

#ifndef ONLINEDRIVESMONITOR_COOLINGSYSTEMMONITOR_H_
#define ONLINEDRIVESMONITOR_COOLINGSYSTEMMONITOR_H_

#include <string>
#include <vector>

#include "drivesmonitor/CoolingMonitorEvent.hh"
#include "stcp.hh"

namespace onlinedrivesmonitor {

class CoolingSystemMonitor
    {
    public:
      CoolingSystemMonitor();
      ~CoolingSystemMonitor();   
      void Configure();
      void Connect(); 
      void Deconnect();
      void ReadData(drivesmonitor::CoolingMonitorEvent *event);
      bool TemperatureWarningIssued();

    private:
      void ConfigureFromDB();
      void DefaultConfiguration();
      std::vector<float> ReadTemperatures(); // [deg Celsius]
      std::vector<float> ReadFlowRates(); // [ml/min]
      float GetChannelReading(const std::size_t& mod, const std::size_t& ch);
      std::string ConstructReadChannelCommand(const std::size_t& mod, const std::size_t& ch);
      void SendCommand(const std::string& cmd);
      std::string ReadReply();
      float ConvertVoltageToFlowRate(const float& voltage); // [ml/min]

      Stcp   socket_;
      std::string ip_; 
      std::string port_;
      int    connection_timeout_;  /// [ms]
      int    read_timeout_; /// [ms]
      int    write_timeout_; /// [ms]
      bool   is_connected_;
      
      std::size_t temp_module_start_addr_;
      std::size_t flow_module_start_addr_;
      std::size_t n_temp_modules_;    
      std::size_t n_temp_channels_per_module_;
      std::size_t n_flow_modules_ ;    
      std::size_t n_flow_channels_per_module_;
            
      float flowrate_calib_line_slope_;
      float flowrate_calib_line_constant_ ;
      float flowrate_calib_min_voltage_;
      float flowrate_calib_max_voltage_;

      float temperature_upper_limit_; /// upper limit when warning is given

      std::vector<float> temperature_readings_;
      std::vector<float> flowrate_readings_;
    };

} // namespace

#endif
