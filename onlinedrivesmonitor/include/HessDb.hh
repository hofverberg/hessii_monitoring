/**
   \class onlinedrivesmonitor::HessDb

   A class to access the DAQ data base. Two functionalities are provided:

   1) Loop over a table specified by the user and gives back all columns that
   was found. Example: return all rows from the table "tablename" that has the
   "Telescope" field name = "1". Notice that a scopeguard must be used to
   properly close the connection to the db and thus correctly release the mutex.

   \code{.cpp}
   HessDb db(tel); // creates a db connection to the telescope tel
   db.Connect();
   db.SetTable("tablename","Telescope");
   std::vector<std::string> table_row_content;
   while (db.FetchNextRowFromTable(&table_row_content);
   // do something with table_row_content
   db.Deconnect();
   \endcode

   2) Get the value of a single parameter in the db. In this case, the
   connection is opened and closed within the function
   and a user thus not have to care about it.

   \code{.cpp}
   HessDb db(tel); // creates a db connection to the telescope tel
   string timeout = db->GetParameter("DriveSetup", "connection_timeout"));
   \endcode

   \author Petter Hofverberg
*/

#ifndef ONLINEDRIVESMONITOR_HESSDB_H_
#define ONLINEDRIVESMONITOR_HESSDB_H_

#include "dbtools/simpletable.hh"

namespace onlinedrivesmonitor {

class HessDb {
 public:
  HessDb();
  virtual ~HessDb();
  /// Connect to the data base with the configuration 'hessdaq'.
  /// Trows if a connection could not be established.
  virtual void Connect();
  virtual void Deconnect();
  virtual bool IsConnected() const;
  /// Returns the value stored in the db for the paramter defined by the
  /// arguments. Throws if the parameter could not be found.
  virtual std::string GetParameter(const std::string &table_name,
				   const std::string &parameter);
  virtual std::string GetParameter(const std::string &table_name,
				   const std::string &id_field_name,
				   const int &id_field_value,
				   const std::string &aux_field1_name,
				   const std::string &aux_field1_value,
				   const std::string &aux_field2_name,
				   const std::string &aux_field2_value,
				   const std::string &parameter);

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  HessDb(const HessDb &handler);
  HessDb &operator=(const HessDb &handler);

    /// Set the table that the class will loop over in subsequent calls to
  /// FetchNextRowFromTable.
  virtual void SetTable(const std::string &table_name);
  /// Set the table that the class will loop over in subsequent calls to
  /// FetchNextRowFromTable. In addition to the above SetTable function,
  /// the returned data will be filtered using the auxiliary fields
  /// @param aux_field1_name filter the data using this parameter
  /// @param aux_field1_value .. that have this value
  /// @param aux_field2_name filter the data using this parameter
  /// @param aux_field2_value .. that have this value
  /// @param condition TODO
  virtual void SetTable(const std::string &table_name,
			const std::string &id_field_name,
			const int &id_field_value,
			const std::string &aux_field1_name,
			const std::string &aux_field1_value,
			const std::string &aux_field2_name,
			const std::string &aux_field2_value,
			const std::string &condition);
  /// Get the next row from the currently set table. Throws if the table is
  /// empty.
  /// @param row_content Vector of strings that will be filled with the table
  /// row content (one item per column).
  /// @return true if a row was found.
  virtual bool FetchNextRowFromTable(
      std::vector<std::string> *row_content);

  simpletable::MySQLconnection mySQL_connection_;
  simpletable::SimpleTable *simpletable_;
};

}  // namespace onlinedrivesmonitor

#endif
