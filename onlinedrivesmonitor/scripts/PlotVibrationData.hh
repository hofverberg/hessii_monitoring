#include <assert.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TLine.h>
#include <TH1.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLegend.h>
#include <sash/Telescope.hh>
#include <sash/HESSArray.hh>
#include "drivesmonitor/VibrationMonitorEvent.hh"

TGraph *gDeviceBackLog;

void FillVibrationData(const std::string &filename, const CrashData::UTC& utc_start, const CrashData::UTC& utc_end);
void SetupGraphs();
void DrawGraphs();

