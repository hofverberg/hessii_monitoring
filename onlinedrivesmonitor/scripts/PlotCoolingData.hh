#include <assert.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TLine.h>
#include <TH1.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLegend.h>
#include <sash/Telescope.hh>
#include <sash/HESSArray.hh>
#include "drivesmonitor/CoolingMonitorEvent.hh"

#define NTEMPCH 8
#define NFLOWCH 4

TGraph *gTemperatures[NTEMPCH];
TGraph *gFlowRates[NFLOWCH];

void FillCoolingData(const std::string &filename, const CrashData::UTC& utc_start, const CrashData::UTC& utc_end);
void SetupGraphs();
void DrawGraphs(const bool& save_gui);
bool TemperatureWarningLevelReached(const double& t_warning_level);
void AddExclDirs(std::vector<std::string>& dirs);
