/*
  Recursively searches the directory "basedir" for all DrivesMonitor root 
  files, fetches the cooling monitor data from these files and plots the 
  temperatures and flowrates versus time for all channels.

  Returns 1 if the temperature at any point exceeded the warning level,
  0 otherwise.

  Example:
    root[0] .include yourhessrootdir/include/
    root[1] gSystem->Load("../../drivesmonitor/lib/librootdrivesmonitor.so")
    root[2] .L ScriptUtils.C+
    root[3] gSystem->Load("ScriptUtils_C.so")
    root[4] .L PlotCoolingData.C+ 
    root[5] PlotCoolingData("/home/hfm/hess/tracking-sync/monitorfiles/DrivesMonitor/") 
*/

#include "PlotCoolingData.hh"
#include "ScriptUtils.hh"
#include <crashdata/UTC.hh>
#include <exception>

using namespace drivesmonitor;

int PlotCoolingData(const std::string& basedir="/home/hfm/hess/tracking-sync/monitorfiles/", // search recursively from this directory
		    const bool& slowcontroldata=true, // include slow control data or not
		    const bool& rundata=true, // include run data or not
		    const std::string& date_start="2014-01-01", // use files only from this date
		    const std::string& date_end="2020-01-01",  // use files only to this data
		    const bool save_gui = false) // save gui to file
{
  gROOT->Reset();
  gROOT->SetStyle("Plain");
  
  SetupGraphs();

  std::cout << "Searching dir " << basedir << " recursively for files" << std::endl;
  std::vector<std::string> files;
  std::vector<std::string> excl_dirs;
  AddExclDirs(excl_dirs);
  GetFiles(files, basedir, excl_dirs, true, slowcontroldata, rundata);
  if (files.empty()) {
    std::cout << "No files found" << std::endl;
    return -1;
  } else {
    std::cout << files.size() << " files found" << std::endl;
  }

  CrashData::UTC utc_start(date_start + " 12:00:00.0");
  CrashData::UTC utc_end(date_end + " 12:00:00.0");

  std::vector<std::string>::iterator it;
  for (it = files.begin(); it != files.end(); ++it) {
    std::cout << "Processing " << *it << " ..." << std::endl;
    FillCoolingData(*it, utc_start, utc_end);
  }

  DrawGraphs(save_gui);

  double t_warning_level(40.0);
  if (TemperatureWarningLevelReached(t_warning_level))
    return 1;
  else
    return 0;
}

void FillCoolingData(const std::string &filename, const CrashData::UTC& utc_start, const CrashData::UTC& utc_end) {
  TFile file(filename.c_str());
  if (file.IsZombie()) {
    std::cerr << "File " << filename << " corrupt, skipping.." << std::endl;
    return;
  }
  TTree *tree = NULL;
  TBranch *event_branch = NULL;
  TBranch *time_branch = NULL;
  
  Sash::HESSArray *hess = &Sash::HESSArray::GetHESSArray();
  //Sash::Telescope* tel = hess->HandleTel(1);
  
  tree = (TTree*)file.Get("CT5_DrivesMonitor_tree");
  if (tree) {
    event_branch = tree->GetBranch("CoolingMonitorEvent_5");
    time_branch = tree->GetBranch("timeStamp");
  }

  if (event_branch == NULL || time_branch == NULL) {
    std::cerr << "Tree missing or corrupt, skipping.." << std::endl;
    return;
  }
  
  // Check that events are in time window
  Sash::Time file_start_time, file_end_time;
  if (!GetTimeInterval(tree, file_start_time, file_end_time)) {
    std::cerr << "Could not get time interval, skipping.." << std::endl;
    return;
  }

  // Assume event time window much smaller than req time window
  Sash::Time req_start_time(utc_start);
  Sash::Time req_end_time(utc_end);
  if (file_start_time < req_start_time || file_end_time > req_end_time) {
    std::cerr << "File outside requested time window, skipping.." << std::endl;
    return;
  }

  CoolingMonitorEvent *cooling_event = 0;
  event_branch->SetAddress(&cooling_event);

  Sash::Time *time_stamp = 0;
  time_branch->SetAddress(&time_stamp);

  int n_entries = tree->GetEntries();
  static int tot_entries = 0; // remember total number of entries in all files
  std::cout << "Number of entries in file: " << n_entries << std::endl;
  for (Int_t i_entry=0; i_entry < n_entries; ++i_entry)
    {
      tree->GetEntry(i_entry);
      std::vector<float> temperatures = cooling_event->GetTemperatures();
      std::vector<float> flow_rates = cooling_event->GetFlowRates();
      double time = time_stamp->GetTimeDouble();
      assert (temperatures.size() == NTEMPCH);
      assert (flow_rates.size() == NFLOWCH);
      for (std::size_t i_ch = 0; i_ch < NTEMPCH; ++i_ch)
	gTemperatures[i_ch]->SetPoint(tot_entries, time, temperatures[i_ch]);
      for (std::size_t i_ch = 0; i_ch < NFLOWCH; ++i_ch)
	gFlowRates[i_ch]->SetPoint(tot_entries, time, flow_rates[i_ch]);
      tot_entries++;
    } 
  return;
}

void SetupGraphs() {
  for (std::size_t i_ch = 0; i_ch < NTEMPCH; ++i_ch){
    gTemperatures[i_ch] = new TGraph(0);
  }
  for (std::size_t i_ch = 0; i_ch < NFLOWCH; ++i_ch){
    gFlowRates[i_ch] = new TGraph(0);
  }
}

void DrawGraphs(const bool& save_gui) {
  TCanvas *cTemperatures = new TCanvas("Temperatures", "Temperatures");
  for (std::size_t i_ch = 0; i_ch < NTEMPCH; ++i_ch){
    gTemperatures[i_ch]->SetMarkerStyle(7);
    gTemperatures[i_ch]->SetMarkerColor(i_ch+1);
    gTemperatures[i_ch]->SetLineColor(i_ch+1);
    if (i_ch == 0) {
      gTemperatures[i_ch]->SetTitle("");
      gTemperatures[i_ch]->GetXaxis()->SetTitle("");
      gTemperatures[i_ch]->GetYaxis()->SetTitle("Temperature [C]");
      gTemperatures[i_ch]->SetMinimum(15.0);
      gTemperatures[i_ch]->SetMaximum(45.0);
      gTemperatures[i_ch]->GetXaxis()->SetTimeDisplay(1);
      gTemperatures[i_ch]->GetXaxis()->SetNdivisions(-503);
      gTemperatures[i_ch]->GetXaxis()->SetTimeFormat("%Y-%m-%d %H:%M"); // %Y-%m
      gTemperatures[i_ch]->GetXaxis()->SetTimeOffset(0,"gmt");
      gTemperatures[i_ch]->Draw("AP");
    }
    else
      gTemperatures[i_ch]->Draw("PSAME");
  }
  TLegend *lTemp = new TLegend(0.2,0.6,0.3,0.8);
  for (std::size_t i_ch = 0; i_ch < NTEMPCH; ++i_ch){
    std::stringstream chx;
    chx << "ch" << i_ch + 1;
    lTemp->AddEntry(gTemperatures[i_ch], chx.str().c_str(), "P");
  }
  lTemp->SetLineWidth(0);
  lTemp->Draw();

  // Flow rates
  TCanvas *cFlowRates = new TCanvas("FlowRates", "FlowRates");
  for (std::size_t i_ch = 0; i_ch < NFLOWCH; ++i_ch){
    gFlowRates[i_ch]->SetMarkerStyle(7);
    gFlowRates[i_ch]->SetMarkerColor(i_ch+1);
    gFlowRates[i_ch]->SetLineColor(i_ch+1);
    if (i_ch == 0) {
      gFlowRates[i_ch]->SetTitle("");
      gFlowRates[i_ch]->GetXaxis()->SetTitle("");
      gFlowRates[i_ch]->GetYaxis()->SetTitle("Flow rate [mL/min]");
      gFlowRates[i_ch]->SetMinimum(-100.0);
      gFlowRates[i_ch]->SetMaximum(20000.0);
      gFlowRates[i_ch]->GetXaxis()->SetTimeDisplay(1);
      gFlowRates[i_ch]->GetXaxis()->SetNdivisions(-503);
      gFlowRates[i_ch]->GetXaxis()->SetTimeFormat("%Y-%m-%d %H:%M"); // %Y-%m
      gFlowRates[i_ch]->GetXaxis()->SetTimeOffset(0,"gmt");
      gFlowRates[i_ch]->Draw("AP");
    }
    else
      gFlowRates[i_ch]->Draw("PSAME");
  }
  TLegend *lFlow = new TLegend(0.2,0.6,0.3,0.8);
  for (std::size_t i_ch = 0; i_ch < NFLOWCH; ++i_ch){
    std::stringstream chx;
    chx << "ch" << i_ch + 1;
    lFlow->AddEntry(gFlowRates[i_ch], chx.str().c_str(), "P");
  }
  lFlow->SetLineWidth(0);
  lFlow->Draw();

  if (save_gui) {
    cTemperatures->SaveAs("Cooling_temperatures.png");
    cFlowRates->SaveAs("Cooling_flowrates.png");
  }
}

bool TemperatureWarningLevelReached(const double& t_warning_level) {
  for (std::size_t i_graph = 0; i_graph < NTEMPCH; ++i_graph) {
    double max_temp = TMath::MaxElement(gTemperatures[i_graph]->GetN(), gTemperatures[i_graph]->GetY());
    if (max_temp > t_warning_level)
      return true;
  }
  return false;
}

void AddExclDirs(std::vector<std::string>& dirs) {
  dirs.push_back("ScanningRadiometer");
  dirs.push_back("Tracking");
  dirs.push_back("SkyCCD");
  dirs.push_back("Analysis");
  dirs.push_back("AutoFocus");
  dirs.push_back("Calibration");
  dirs.push_back("Camera");
  dirs.push_back("CameraRate");
  dirs.push_back("Ceilometer");
  dirs.push_back("centraltrigger_logs");
  dirs.push_back("DurhamLidar");
  dirs.push_back("Trigger");
  dirs.push_back("FlatField");
  dirs.push_back("Lidar");
  dirs.push_back("LidCCD");
  dirs.push_back("Load");
  dirs.push_back("Meteo");
  dirs.push_back("OutdoorCalibration");
  dirs.push_back("Radiometer");
  dirs.push_back("StickyBitFixer");
  dirs.push_back("Transmissometer");
}
