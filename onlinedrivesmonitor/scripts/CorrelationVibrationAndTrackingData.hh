#include <assert.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TLine.h>
#include <TH1.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TH2F.h>
#include <sash/Telescope.hh>
#include <sash/HESSArray.hh>
#include "drivesmonitor/VibrationMonitorEvent.hh"
#include "tracking2/Tracking2Event.hh"

void ProcessFile(const std::string &drivesmonitor_file, const std::string& tracking_file, const CrashData::UTC& utc_start, const CrashData::UTC& utc_end);
void SetupGraphs();
void DrawGraphs();

TH2F *hBacklogVSAzPos;
