#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#include <TTree.h>
#include <TBranch.h>
#include <sash/HESSArray.hh>

void GetFiles(std::vector<std::string> &files, 
	      std::string dir, 
	      std::vector<std::string> &excl_dirs,
	      bool recursive, 
	      bool slowcontroldata,
	      bool rundata);
// Get the time of the first and the last entry of the given TTree
bool GetTimeInterval(TTree* tree, Sash::Time& start_time, Sash::Time& end_time);
// Starts searching the TTree tree" at the entry "event_number" for an event with
// a time stamp "time_stamp". If it finds it, "event_number" is updated with the new
// event number and the function returns true. If it doesnt find it, false is returned
// and "event_number" is not updated.
bool GetEventNumberAtTimeStamp(TTree* tree, const Sash::Time& time_stamp, int& event_number);
// run files are saved in monitorfiles/run***/
// and have the format: run_102297_DrivesMonitor_001.root 
bool IsRunFile(const std::string& filename);
// slow control files are saved in monitorfiles/DrivesMonitor/
// and have the format: DrivesMonitor_2014-12-15_001.root
bool IsSlowControlFile(const std::string& filename);
std::string GetCorrespondingTrackingFile(const std::string& drivesmonitor_file);
