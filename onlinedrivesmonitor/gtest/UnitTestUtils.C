/**
   \file UnitTestUtils
   
   Unit tests for the Utils functions.

   \author Faical Ait Benkhali, Petter Hofverberg
 */

#include <string>
#include <exception>

#include "Utils.hh"

using namespace ::testing;
using namespace onlinedrivesmonitor;

class UnitTestUtils : public ::testing::Test {

 protected:

  virtual void SetUp() {}

  virtual void TearDown() {}
};

TEST_F(UnitTestUtils, BoolInStringFormConvertsCorrectlyToBool) {
  ASSERT_THAT(StringToBool("true"), Eq(true));
  ASSERT_THAT(StringToBool("True"), Eq(true));
  ASSERT_THAT(StringToBool("TRUE"), Eq(true));
  ASSERT_THAT(StringToBool("false"), Eq(false));
  ASSERT_THAT(StringToBool("False"), Eq(false));
  ASSERT_THAT(StringToBool("FALSE"), Eq(false));
}

TEST_F(UnitTestUtils, NonBoolInStringFormThrowsWhenBeingConvertedToBool) {
  ASSERT_THROW(StringToBool(""), std::exception);
  ASSERT_THROW(StringToBool("truen"), std::exception);
  ASSERT_THROW(StringToBool("atrue"), std::exception);
  ASSERT_THROW(StringToBool("falsen"), std::exception);
  ASSERT_THROW(StringToBool("afalse"), std::exception);
}

TEST_F(UnitTestUtils, IntInStringFormConvertsCorrectlyToInt) {
  std::string an_int("10");
  ASSERT_THAT(StringToInt(an_int), Eq(10));
}

TEST_F(UnitTestUtils, NonIntInStringFormThrowsWhenTryingToBeConvertedToInt) {
  std::string not_an_int("mupp");
  ASSERT_THROW(StringToInt(not_an_int), std::exception);
}

TEST_F(UnitTestUtils, DoubleInStringFormConvertsCorrectlyToDouble) {
  std::string a_double("10.0001");
  ASSERT_THAT(StringToDouble(a_double), DoubleEq(10.0001));
}

TEST_F(UnitTestUtils, NonDoubleInStringFormThrowsWhenTryingToBeConvertedToDouble) {
  std::string not_a_double("mupp");
  ASSERT_THROW(StringToDouble(not_a_double), std::exception);
}

TEST_F(UnitTestUtils, FloatInStringFormConvertsCorrectlyToFloat) {
  std::string a_float("10.0001");
  ASSERT_THAT(StringToFloat(a_float), FloatEq(10.0001));
}

TEST_F(UnitTestUtils, NonFloatInStringFormThrowsWhenTryingToBeConvertedToFloat) {
  std::string not_a_float("mupp");
  ASSERT_THROW(StringToFloat(not_a_float), std::exception);
}

TEST_F(UnitTestUtils, StringToVectorThrowsWhenGivenAnEmtpyString) {
  std::string str("");
  std::string delim(",");
  ASSERT_THROW(StringToVector(str,delim), std::exception);
}

TEST_F(UnitTestUtils, StringToVectorThrowsWhenGivenAnEmtpyDeliminator) {
  std::string str("hej");
  std::string delim("");
  ASSERT_THROW(StringToVector(str,delim), std::exception);
}

TEST_F(UnitTestUtils, StringToVectorConvertsCorrectlyASingleElement) {
  std::string str("Hej");
  std::string delim(",");
  std::vector<std::string> vec = StringToVector(str,delim);
  ASSERT_THAT(vec, ElementsAre("Hej"));
}

TEST_F(UnitTestUtils, StringToVectorConvertsCorrectlyACommanDeliminatedString) {
  std::string str("Hej,din,gamle");
  std::string delim(",");
  std::vector<std::string> vec = StringToVector(str,delim);
  ASSERT_THAT(vec, ElementsAre("Hej","din","gamle"));
}

TEST_F(UnitTestUtils, StringToVectorConvertsCorrectlyACommanDeliminatedStringWithSpaces) {
  std::string str(" Hej,din ,  gamle");
  std::string delim(",");
  std::vector<std::string> vec = StringToVector(str,delim);
  ASSERT_THAT(vec, ElementsAre("Hej","din","gamle"));
}
