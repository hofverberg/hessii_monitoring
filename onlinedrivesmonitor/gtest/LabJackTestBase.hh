/**
   \file LabJackTestBase

   Super class defining common parameters and expectations of unit tests of the
   LabJack classes.

   \author Faical Ait Benkhali, Petter Hofverberg
 */

#ifndef ONLINEDRIVESMONITOR_LABJACKTESTBASE_H_
#define ONLINEDRIVESMONITOR_LABJACKTESTBASE_H_

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <sys/time.h>
#include <time.h>

#include "LabJackM.h"
#include "LJM_Utilities.hh"

using namespace ::testing;
using namespace onlinedrivesmonitor;

class LabJackTestBase : public ::testing::Test {
private:
  int connection_;

 protected:

  virtual void SetUp();
  virtual void TearDown();

  std::string Connect();
  std::string Deconnect();
  std::string GetLJMError(const int& ljm_error_code);
  std::string LoadConfiguration();
  double GetUSecTimeStamp();

  int connection() { return connection_; }
};

#endif
