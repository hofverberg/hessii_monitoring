#ifndef DRIVESMONITOR_VIBRATIONMONITOREVENT_HH_
#define DRIVESMONITOR_VIBRATIONMONITOREVENT_HH_

#include <sash/MonitorBase.hh>
#include <sash/Telescope.hh>

namespace drivesmonitor {

  class VibrationMonitorEvent : public Sash::MonitorBase
    {
    public:
      VibrationMonitorEvent(const Sash::Telescope& 
#if defined(__CINT__) || defined(CINTOBJECT)
	    = dummy
#endif
	    ,const char *name = "");
      virtual ~VibrationMonitorEvent();
      
      GETMEMBER(Vibrations, std::vector<double>); ///< Get all channels interleaved
      GETMEMBER(DeviceScanBackLog, int);
      GETMEMBER(LJMScanBackLog, int);
      GETMEMBER(Channels, std::vector<std::string>);
      GETMEMBER(ScansPerRead, int);
      GETMEMBER(DesScanRate, double);
      GETMEMBER(ActScanRate, double);

      std::vector<double> GetVibrationsSingleChannel(const std::string& ch); ///< Get data for one channel. Throws if channel doesnt exist.

    private:
      std::vector<double> fVibrations; ///< Bad vibrations
      int fDeviceScanBackLog;
      int fLJMScanBackLog;
      std::vector<std::string> fChannels;
      int fScansPerRead;
      double fDesScanRate;
      double fActScanRate;

      const Sash::Telescope* fTelescope; //!
             
      ClassDef(drivesmonitor::VibrationMonitorEvent,1)   
  };

} // namespace

#if defined(__CINT__) || defined(CINTOBJECT)

#include <sash/EnvelopeEntry.hh>

TBuffer& operator>>(TBuffer &,
		    Sash::EnvelopeEntry<Sash::Telescope,
		    drivesmonitor::VibrationMonitorEvent> *&);
#endif

#endif
