#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ namespace drivesmonitor;


#pragma link C++ class drivesmonitor::CoolingMonitorEvent+;
#pragma link C++ class drivesmonitor::VibrationMonitorEvent+;

#ifdef ROOT_HAVE_TEMPLATE_MEMBER

// CoolingMonitorEvent
#pragma link C++ function Sash::Telescope::Get(drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::Telescope::Handle(drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::Telescope::Get(const char *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::Telescope::Handle(const char *,drivesmonitor::CoolingMonitorEvent *);

#pragma link C++ function Sash::HESSArray::Get(unsigned,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::HESSArray::Handle(unsigned,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::HESSArray::Get(unsigned,const char *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::HESSArray::Handle(unsigned,const char *,drivesmonitor::CoolingMonitorEvent *);

#pragma link C++ function Sash::Telescope::UnHandle(drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::Telescope::UnHandle(const char *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::HESSArray::UnHandle(unsigned,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Sash::HESSArray::UnHandle(unsigned,const char *,drivesmonitor::CoolingMonitorEvent *);

// VibrationMonitorEvent
#pragma link C++ function Sash::Telescope::Get(drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::Telescope::Handle(drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::Telescope::Get(const char *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::Telescope::Handle(const char *,drivesmonitor::VibrationMonitorEvent *);

#pragma link C++ function Sash::HESSArray::Get(unsigned,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::HESSArray::Handle(unsigned,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::HESSArray::Get(unsigned,const char *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::HESSArray::Handle(unsigned,const char *,drivesmonitor::VibrationMonitorEvent *);

#pragma link C++ function Sash::Telescope::UnHandle(drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::Telescope::UnHandle(const char *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::HESSArray::UnHandle(unsigned,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Sash::HESSArray::UnHandle(unsigned,const char *,drivesmonitor::VibrationMonitorEvent *);
				  
#else

// CoolingMonitorEvent
#pragma link C++ function Get(Sash::Telescope *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Handle(Sash::Telescope *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Get(Sash::Telescope *,const char *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function Handle(Sash::Telescope *,const char *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function UnHandle(Sash::Telescope *,drivesmonitor::CoolingMonitorEvent *);
#pragma link C++ function UnHandle(Sash::Telescope *,const char *,drivesmonitor::CoolingMonitorEvent *);

// VibrationMonitorEvent
#pragma link C++ function Get(Sash::Telescope *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Handle(Sash::Telescope *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Get(Sash::Telescope *,const char *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function Handle(Sash::Telescope *,const char *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function UnHandle(Sash::Telescope *,drivesmonitor::VibrationMonitorEvent *);
#pragma link C++ function UnHandle(Sash::Telescope *,const char *,drivesmonitor::VibrationMonitorEvent *);

#endif

#endif
