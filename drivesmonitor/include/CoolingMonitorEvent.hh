#ifndef DRIVESMONITOR_COOLINGMONITOREVENT_HH_
#define DRIVESMONITOR_COOLINGMONITOREVENT_HH_

#include <sash/MonitorBase.hh>
#include <sash/Telescope.hh>
#include <TArrayD.h>

namespace drivesmonitor {

  class CoolingMonitorEvent : public Sash::MonitorBase
    {
    public:
      CoolingMonitorEvent(const Sash::Telescope& 
#if defined(__CINT__) || defined(CINTOBJECT)
	    = dummy
#endif
	    ,const char *name = "");
      virtual ~CoolingMonitorEvent();
      
      //void Copy(TObject &obj) const;

      //virtual void print(std::ostream&) const;
      
      GETMEMBER(Temperatures, std::vector<float>);
      GETMEMBER(FlowRates, std::vector<float>); 

      TArrayD GetTemperaturesAsTArray(); ///< for display, which requires TArrayD
      TArrayD GetFlowRatesAsTArray(); ///< for display, which requires TArrayD

    private:
      std::vector<float> fTemperatures; ///< Temperatures, currently 8 ch
      std::vector<float> fFlowRates; ///< Flow rates, currently 4 ch

      const Sash::Telescope* fTelescope; //!
             
      ClassDef(drivesmonitor::CoolingMonitorEvent,1)   
  };

} // namespace

#if defined(__CINT__) || defined(CINTOBJECT)

#include <sash/EnvelopeEntry.hh>

TBuffer& operator>>(TBuffer &,
		    Sash::EnvelopeEntry<Sash::Telescope,
		    drivesmonitor::CoolingMonitorEvent> *&);
#endif

#endif
